<!DOCTYPE html>
<html lang="en">
<head>
    <title>United Builders</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">

    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset("css/owl.theme.default.min.css")}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery.timepicker.css')}}">


    <link rel="stylesheet" href="{{asset('css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!--close button-->
    <script>
        $(window).scroll(function () {
            if ($(document).scrollTop() >= $(document).height() / 6.35)
                $("#spopup").show("slow"); else $("#spopup").hide("slow");
        });

        function closeSPopup() {
            $('#spopup').hide('slow');
        }
    </script>
    <div id="spopup" style="display: none;">
        <div class="modal-content">
            {{--<a style="position:absolute;top:14px;right:10px;color:#ffffff;font-size:10px;font-weight:bold;"
               href="javascript:void(0);" onclick="return closeSPopup();">
            </a>--}}
            <form method="post" action="{{route('subscribeStore')}}">
                @csrf
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        @if(is_array(session()->get('success')))
                            <ul>
                                @foreach (session()->get('success') as $message)
                                    <li style="color: black">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @else
                            {{ session()->get('success') }}
                        @endif
                    </div>
                @endif
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel1">Contact Us</h6>
                </div>
                <div class="modal-body">

                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }}>
                        {{--<label for="name" class="control-label">Name:</label>--}}
                        <input type="text" class="form-control" placeholder="Your Name *" id="name" name="name"
                               required>
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>

                    <div class="form-group" {{ $errors->has('phone') ? 'has-error' : '' }}>
                        {{--<label for="phone" class="control-label">Phone:</label>--}}
                        <input type="number" min="0" class="form-control" placeholder="Phone Number *" id="phone"
                               name="phone" required>
                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" href="javascript:void(0);" onclick="return closeSPopup();"
                            class="btn btn-danger" data-dismiss="modal">Close
                    </button>
                    <button type="button" onclick="submit('subscribe')" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!--close button-->


        <!--insert popup content here-->
    </div>
    <style>
        #spopup {
            background: rgba(0, 0, 0, 0);
            border-radius: 9px;
            -moz-border-radius: 9px;
            -webkit-border-radius: 9px;
            -moz-box-shadow: inset 0 0 3px #333;
            /* -webkit-box-shadow: inset 0 0 3px #333; */
            padding: 5px 5px 5px 5px;
            width: 275px;
            height: 350px;
            position: fixed;
            bottom: 15px;
            /* right: 2px; */
            display: none;
            z-index: 90;
        }
    </style>
    <style>
        .popup-btn a {
            top: 320px;
            position: fixed;
            right: -90px;
            z-index: 1000;
            transform: rotate(-90deg);
            background-color: red;
            padding: 10px 60px 35px;
            height: 0px;
            background-color: #4267b2;
            color: #fff;
        }

        .popup-btn a:hover {
            text-decoration: none;
            color: #fff;
        }

        .form-group {
            margin-bottom: 0rem;
        }
    </style>

</head>
<body>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00"/>
    </svg>
</div>

<!-- Property Search Section For Desktop View -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-12">
            <div class="popup-btn">
                <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#form">Contact Us</a>
            </div>

            <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header border-bottom-0">
                            <h5 class="modal-title" id="exampleModalLabel">Enquiry Form</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @if (count($errors)>0)
                            @foreach($errors->all() as $error)
                                <li class=" alert-danger">{{$error}}</li><br>
                            @endforeach
                        @endif
                        <form action="{{route('enquiryStore')}}" method="post">
                            @csrf
                            @if (session()->has('success'))
                                <div class="alert alert-success">
                                    @if(is_array(session()->get('success')))
                                        <ul>
                                            @foreach (session()->get('success') as $message)
                                                <li style="color: black">{{ $message }}</li>
                                            @endforeach
                                        </ul>
                                    @else
                                        {{ session()->get('success') }}
                                    @endif
                                </div>
                            @endif
                            <div class="modal-body">
                                <div class="form-group row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-3">
                                        <label for="email1">First Name</label>
                                        <input type="text" name="f_name" class="form-control" placeholder="First Name"
                                               required>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-3">
                                        <label for="password1">Last Name</label>
                                        <input type="text" name="l_name" class="form-control" placeholder="Last Name"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email1">Phone Number</label>
                                    <input type="number" name="phone" class="form-control" placeholder="Phone" required>
                                </div>
                                <br>
                                <div class="form-group row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-3">
                                        <select name="service_type" id="" class="form-control" required>
                                            <option value="">Select Your Services</option>
                                            <option value="Building Plan Design 2D,3D">Building Plan Design 2D,3D
                                            </option>
                                            <option value="Building Estimation">Building Estimation</option>
                                            <option value="Construction">Construction</option>
                                            <option value="Renovation">Renovation</option>
                                            <option value="Interior/Exterior Design">Interior/Exterior Design</option>
                                            <option value="Electrical Wiring">Electrical Wiring</option>
                                            <option value="Painting/Plumbing">Painting / Plumbing</option>
                                        </select>
                                    </div>

                                    <!-- Form Group -->

                                </div>
                                <div class="modal-footer border-top-0 d-flex justify-content-center">
                                    <button type="submit" class="theme-btn btn-style-one"><span class="btn-title">Submit Now</span>
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Property Search Section -->


@include('frontend.component.header')
@yield('content')
@include('frontend.component.footer')

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            @if (count($errors)>0)
                @foreach($errors->all() as $error)
                    <li class=" alert-danger">{{$error}}</li><br>
                @endforeach
            @endif
            <form method="post" action="{{route('enquiryStore')}}" {{--id="demo-form"--}}>
                @csrf
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        @if(is_array(session()->get('success')))
                            <ul>
                                @foreach (session()->get('success') as $message)
                                    <li style="color: black">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @else
                            {{ session()->get('success') }}
                        @endif
                    </div>
                @endif

                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Submit Enquiry</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="name" class="control-label">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="control-label">Phone:</label>
                        <input type="number" min="0" class="form-control" id="phone" name="phone" required>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Message:</label>
                        <textarea class="form-control" id="message-text1" name="message"></textarea>
                    </div>


                </div>
                {{-- <div class="form-group">
                     <div class="g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}"></div>
                     @if($errors->has('g-recaptcha-response'))
                         <span class="invalid-feedback" style="display: block">
                             <strong>{{$errors->first('g-recaptcha-response')}}</strong>
                         </span>
                     @endif
                     </div>--}}
                {{--<div class="form-group col-md-4">
                            <div class="g-recaptcha"
                                 data-sitekey="6Ldi6-8UAAAAABf0XugVlpSh85fRnYWw79YbG0Bf">

                            </div>
                        --}}{{--<label for="captcha">Captcha</label>
                        {!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}--}}{{--
                       --}}{{-- <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>--}}{{--
                </div>--}}
                <div id='recaptcha' class="g-recaptcha"
                     data-sitekey="6LcXY_AUAAAAAD0jEXN7Abgyzq-FIFUGGJMFHlOk"
                     data-callback="onSubmit"
                     data-size="invisible">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id='submit' class="btn btn-success">Submit</button>
                    {{-- <button class="btn btn-success" data-sitekey="6LdgMfAUAAAAAOosL_GlBaOpJoTWKRhkXR_VE3hw" data-callback='onSubmit'>Submit</button>
 --}}
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/aos.js')}}"></script>
<script src="{{asset('js/jquery.animateNumber.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/jquery.timepicker.min.js')}}"></script>
<script src="{{asset('js/scrollax.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{asset('js/google-map.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>


<script type="text/javascript">
    (function () {
        var options = {
            facebook: "268316410790457", // Facebook page ID
            call_to_action: "Live Chat", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>

</body>
</html>
