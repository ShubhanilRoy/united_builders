@extends('frontend.base')
@section('content')


    <section class="home-slider owl-carousel">
        <div class="slider-item" style="background-image:url(images/bg_1.jpg);" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                    <div class="col-md-6 text ftco-animate pl-md-5">
                        <h1 class="mb-4">Base Construction <span>Build The Future</span></h1>
                        <h3 class="subheading">A small river named Duden flows by their place and supplies it with the necessary regelialia</h3>
                        <p><a  data-toggle="modal" data-target="#exampleModal"
                              data-whatever="@mdo" class="btn btn-secondary px-4 py-3 mt-3">Request A Quote</a></p>

                    </div>
                </div>
            </div>
        </div>

        <div class="slider-item" style="background-image:url(images/bg_2.jpg);" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                    <div class="col-md-6 text ftco-animate pl-md-5">
                        <h1 class="mb-4">We Turn Your <span>Vision Into Reality</span></h1>
                        <h3 class="subheading">A small river named Duden flows by their place and supplies it with the necessary regelialia</h3>
                        <p><a  data-toggle="modal" data-target="#exampleModal"
                               data-whatever="@mdo" class="btn btn-secondary px-4 py-3 mt-3">Request A Quote</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-section ftco-no-pt ftco-margin-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="request-quote">
                        <div class="bg-primary py-4">
                            <span class="subheading">Be Part of our Business</span>
                            <h3>Request A Quote</h3>
                        </div>
                        <form action="{{route('enquiryStore')}}" class="request-form ftco-animate" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="f_name" class="form-control" placeholder="First Name" required>
                            </div>
                            <div class="form-group">
                                <input type="text"  name="l_name" class="form-control" placeholder="Last Name" required>
                            </div>
                            <div class="form-group">
                                <div class="form-field">
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="service_type" id="" class="form-control" required>
                                            <option value="">Select Your Services</option>
                                            <option value="Building Plan Design 2D,3D">Building Plan Design 2D,3D</option>
                                            <option value="Building Estimation">Building Estimation</option>
                                            <option value="Construction">Construction</option>
                                            <option value="Renovation">Renovation</option>
                                            <option value="Interior/Exterior Design">Interior/Exterior Design</option>
                                            <option value="Electrical Wiring">Electrical Wiring</option>
                                            <option value="Painting/Plumbing">Painting / Plumbing</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="number" name="phone" class="form-control" placeholder="Phone" required>
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="" cols="30" rows="2" class="form-control" placeholder="Message"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Enquiry Now" class="btn btn-primary py-3 px-4">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8 wrap-about py-5 ftco-animate">
                    <div class="heading-section mb-5">
                        <h2 class="mb-4">We Are Highly Recommendable Construction Firm</h2>
                    </div>
                    <div class="">
                        <p class="mb-5">On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
                        <p><a href="#" class="btn btn-secondary px-5 py-3">Read More</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-services ftco-no-pt">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-2">
                <div class="col-md-8 text-center heading-section ftco-animate">
                    <span class="subheading">Services</span>
                    <h2 class="mb-4">Our Services</h2>
                    <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
                    <div class="media block-6 d-block text-center">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="flaticon-home"></span>
                        </div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading">Architecture Design</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
                    <div class="media block-6 d-block text-center">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="flaticon-hook"></span>
                        </div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading">Construction/Renovation</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
                    <div class="media block-6 d-block text-center">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="flaticon-skyline"></span>
                        </div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading">House Renovation</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
                    <div class="media block-6 d-block text-center">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="flaticon-stairs"></span>
                        </div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading">Electrical Work</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>


    <section class="ftco-intro" style="background-image: url(images/bg_3.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9 text-center">
                    <h2>Lets Build Your Dream Together</h2>
                    <p>We can manage your dream building A small river named Duden flows by their place</p>
                    <p class="mb-0">
                        <a data-toggle="modal" data-target="#exampleModal"
                                       data-whatever="@mdo" class="btn btn-secondary px-4 py-3 mt-3">Know more about us</a>
                    </p>
                </div>
            </div>
        </div>
    </section>



    <section class="ftco-section ftco-no-pt ftco-no-pb">
        <div class="container-fluid p-0">
            <div class="row no-gutters justify-content-center mb-5 pb-2">
                <div class="col-md-6 text-center heading-section ftco-animate">
                    <span class="subheading">Projects</span>
                    <h2 class="mb-4">Featured Projects</h2>
                    <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                        <img src="images/work-1.jpg" class="img-fluid" alt="Colorlib Template">
                        <div class="text">
                            <span>Commercial</span>
                            <h3><a href="project.html">San Francisco Tower</a></h3>
                        </div>
                        <a href="images/work-1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                            <span class="icon-expand"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                        <img src="images/work-2.jpg" class="img-fluid" alt="Colorlib Template">
                        <div class="text">
                            <span>Commercial</span>
                            <h3><a href="project.html">San Francisco Tower</a></h3>
                        </div>
                        <a href="images/work-2.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                            <span class="icon-expand"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                        <img src="images/work-3.jpg" class="img-fluid" alt="Colorlib Template">
                        <div class="text">
                            <span>Commercial</span>
                            <h3><a href="project.html">San Francisco Tower</a></h3>
                        </div>
                        <a href="images/work-3.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                            <span class="icon-expand"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                        <img src="images/work-4.jpg" class="img-fluid" alt="Colorlib Template">
                        <div class="text">
                            <span>Commercial</span>
                            <h3><a href="project.html">San Francisco Tower</a></h3>
                        </div>
                        <a href="images/work-4.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                            <span class="icon-expand"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                        <img src="images/work-5.jpg" class="img-fluid" alt="Colorlib Template">
                        <div class="text">
                            <span>Commercial</span>
                            <h3><a href="project.html">San Francisco Tower</a></h3>
                        </div>
                        <a href="images/work-5.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                            <span class="icon-expand"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                        <img src="images/work-6.jpg" class="img-fluid" alt="Colorlib Template">
                        <div class="text">
                            <span>Resedencial</span>
                            <h3><a href="project.html">Rose Villa House</a></h3>
                        </div>
                        <a href="images/work-6.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                            <span class="icon-expand"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                        <img src="images/work-7.jpg" class="img-fluid" alt="Colorlib Template">
                        <div class="text">
                            <span>Commercial</span>
                            <h3><a href="project.html">San Francisco Tower</a></h3>
                        </div>
                        <a href="images/work-7.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                            <span class="icon-expand"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                        <img src="images/work-8.jpg" class="img-fluid" alt="Colorlib Template">
                        <div class="text">
                            <span>Commercial</span>
                            <h3><a href="project.html">San Francisco Tower</a></h3>
                        </div>
                        <a href="images/work-8.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                            <span class="icon-expand"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-counter img" id="section-counter" style="background-image: url(images/bg_3.jpg);" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 d-flex">
                        <div class="text d-flex align-items-center">
                            <strong class="number" data-number="5">0</strong>
                        </div>
                        <div class="text-2">
                            <span>Years of <br>Experienced</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 d-flex">
                        <div class="text d-flex align-items-center">
                            <strong class="number" data-number="21">0</strong>
                        </div>
                        <div class="text-2">
                            <span>Project <br>Successful</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 d-flex">
                        <div class="text d-flex align-items-center">
                            <strong class="number" data-number="35">0</strong>
                        </div>
                        <div class="text-2">
                            <span>Professional <br>Expert</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 d-flex">
                        <div class="text d-flex align-items-center">
                            <strong class="number" data-number="21">0</strong>
                        </div>
                        <div class="text-2">
                            <span>Happy <br>Customers</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section testimony-section">
        <div class="container">
            <div class="row ftco-animate">
                <div class="col-md-6 col-lg-6 col-xl-4">
                    <div class="heading-section ftco-animate">
                        <span class="subheading">Services</span>
                        <h2 class="mb-4">Experience Great Services</h2>
                    </div>
                    <div class="services-flow">
                        <div class="services-2 p-4 d-flex ftco-animate">
                            <div class="icon">
                                <span class="flaticon-engineer"></span>
                            </div>
                            <div class="text">
                                <h3>Expert &amp; Professional</h3>
                                <p>Separated they live in. A small river named Duden flows</p>
                            </div>
                        </div>
                        <div class="services-2 p-4 d-flex ftco-animate">
                            <div class="icon">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="text">
                                <h3>High Quality Work</h3>
                                <p>Separated they live in. A small river named Duden flows</p>
                            </div>
                        </div>
                        <div class="services-2 p-4 d-flex ftco-animate">
                            <div class="icon">
                                <span class="flaticon-engineer-2"></span>
                            </div>
                            <div class="text">
                                <h3>24/7 Help Support</h3>
                                <p>Separated they live in. A small river named Duden flows</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 d-xl-block d-none"></div>
                <div class="col-md-6 col-lg-6 col-xl-7">
                    <div class="heading-section ftco-animate mb-5">
                        <span class="subheading">Testimonials</span>
                        <h2 class="mb-4">Satisfied Customer</h2>
                        <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
                    </div>
                    <div class="carousel-testimony owl-carousel">
                        <div class="item">
                            <div class="testimony-wrap">
                                <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Racky Henderson</p>
                                    <span class="position">Farmer</span>
                                </div>
                                <div class="user-img" style="background-image: url(images/person_1.jpg)">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-wrap">
                                <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Henry Dee</p>
                                    <span class="position">Businessman</span>
                                </div>
                                <div class="user-img" style="background-image: url(images/person_2.jpg)">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-wrap">
                                <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Mark Huff</p>
                                    <span class="position">Students</span>
                                </div>
                                <div class="user-img" style="background-image: url(images/person_3.jpg)">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-wrap">
                                <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Rodel Golez</p>
                                    <span class="position">Striper</span>
                                </div>
                                <div class="user-img" style="background-image: url(images/person_4.jpg)">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-wrap">
                                <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Ken Bosh</p>
                                    <span class="position">Manager</span>
                                </div>
                                <div class="user-img" style="background-image: url(images/person_1.jpg)">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@stop
