<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2 logo"><span class="flaticon-bee"></span>United Builds</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>

            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-5 ml-md-4">
                    <h2 class="ftco-heading-2">Services</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Construction</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Building Plan Design</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Renovation</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Painting/Plumbing</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Electrical Wiring</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Interior Design</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Exterior Design</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2">Be With Us</h2>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/United-Builders-268316410790457/"
                         data-tabs="timeline" data-height="250" data-small-header="false"
                         data-adapt-container-width="true" data-hide-cover="false"
                         data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/United-Builders-268316410790457/"
                                    class="fb-xfbml-parse-ignore">
                            <a href="https://www.facebook.com/United-Builders-268316410790457/">United Builders</a></blockquote>
                    </div>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2">Newsletter</h2>
                    <form action="{{route('subscribeStore')}}" method="post" class="subscribe-form">
                        @csrf
                        <div class="form-group">
                            <input type="text" required name="email" class="form-control mb-2 text-center" placeholder="Enter email address">
                            <input type="submit" value="Subscribe" class="form-control submit px-3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p>
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="http://www.unitedbuilds.com/" target="_blank">United Infotech</a>
                  </p>
            </div>
        </div>
    </div>
</footer>
