<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Model\Enquiry;
use Illuminate\Http\Request;

class EnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function enquiryStore(Request $request)
    {
        $this->validate($request,[

            'f_name' => 'required',
            'l_name' => 'required',
            'phone' => 'required|numeric',

        ],[

            'f_name.required' => ' The First Name field is required.',
            'l_name.required' => ' The Last Name field is required.',
            'phone.required' => ' The Contact Number field is required.',

        ]);
        $add = new Enquiry();
        $add->f_name = $request->f_name;
        $add->l_name = $request->l_name;
        $add->phone = $request->phone;
        $add->email = $request->email;
        $add->message = $request->message;
        $add->service_type = $request->service_type;
        $add->save();

        return back()->withSuccess([
            'Congratulations!! Your message has been Received. We Will Reach You Shortly.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
