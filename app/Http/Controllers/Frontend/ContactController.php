<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Model\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function contactStore(Request $request)
    {
        $this->validate($request,[

            'f_name' => 'required',
            /*'l_name' => 'required',*/
            'phone' => 'required|numeric',

        ],[

            'f_name.required' => ' The First Name field is required.',
          /*  'l_name.required' => ' The Last Name field is required.',*/
            'phone.required' => ' The Contact Number field is required.',

        ]);
        $add = new Contact();
        $add->f_name = $request->f_name;
        $add->l_name = $request->l_name;
        $add->phone = $request->phone;
        $add->email = $request->email;
        $add->message = $request->message;
        $add->subject = $request->subject;
        $add->save();

        return back()->withSuccess([
            'Congratulations!! Your message has been Received. We Will Reach You Shortly.'
        ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
