<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
       return view('frontend.pages.index');
    }
    public function about()
    {
       return view('frontend.pages.about');
    }

    public function contact()
    {
       return view('frontend.pages.contact');
    }

    public function service()
    {
       return view('frontend.pages.service');
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
